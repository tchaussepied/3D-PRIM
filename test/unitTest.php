<?php
#require_once('connexionDB.php');
use PHPUnit\Framework\TestCase;


function isConnected($servername, $username, $password){
    $connexion_db =  mysqli_connect($servername, $username, $password);
    
    if (!$connexion_db) {
        return 0;
    }
    else{
        return 1;
    }
}

function request($idItem,$servername, $username, $password, $database){
    
    $connexion_db =  mysqli_connect($servername, $username, $password);

    $sql = 'SELECT * FROM '.$database.'.Materiel WHERE idItem ="'.$idItem.'"';
    $query = mysqli_query($connexion_db,$sql);
    $result= mysqli_fetch_array($query);

    return $result['type_fil'];
}

class DbRequestTest extends TestCase
{

    /**
     * @requires extension mysqli
    */
    public function testConnexionDB()
    {
        $servername = 'localhost:9306';
        $username = 'thomas';
        $password = 'C8a21s19e16!&d';
        $isConnexion=isConnected($servername, $username, $password);
        $this->assertSame(1,$isConnexion);
    }

    /**
     * @requires extension mysqli
    */
     public function testRequest(){
        $servername = 'localhost:9306';
        $username = 'thomas';
        $password = 'C8a21s19e16!&d';
        $database = 'mydb';

        $result=request("6",$servername, $username, $password, $database);
        $this->assertSame("PLA",$result);
    }


    /**
     * @expectedException PHPUnit\Framework\Error\Error
     */
    public function testFailingInclude()
    {
        include 'not_existing_file.php';
    }
}

?>
