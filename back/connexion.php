<?php
    ini_set('display_errors', 1);
    require("fonctions.php");
    onglets_menu_html();

?>

<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Connexion</title>
    </head>
    <body>      
        <div class="connexion">
            <form method="post" action="connexion_utilisateur.php" name="connexion">
                    
                <label for="pseudo">Pseudo : </label>
                <input type="text" placeholder="Votre pseudo" name="pseudo" value="" required><br>
                <label for="mdp">Mot de passe : </label>
                <input type="password" placeholder="Mot de passe" name="mdp" value="" required><br>
                <button type="submit" name="connexion">Se connecter</button><br>
            </form>
        </div>
    </body>
</html>