<?php
    ini_set('display_errors', 1);


function onglets_menu_html(){
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>3D-PRIM ADMINSITRATION</title>
            <link rel="stylesheet" href="style.css">

        </head>
        <body>
            <header>
                <h1>3D-PRIM, ADMINSITRATION</h1>
            </header>
           
            <div id="mySidenav" class="sidenav">
                <?php
                    if (isset($_SESSION['id']) AND isset($_SESSION['pseudo'])){
                        echo 'Bonjour ' . $_SESSION['pseudo'];
                        ?><br><br>
                        <a id="home" href="index.php">Home</a>
                        <br><br>
                        <a id="user" href="admin.php">Votre page perso</a>
                        <a id="insc" href="inscription.php">Nouvel administrateur</a>

                        <a id="deco" href="deconnexion.php">Déconnexion</a>
                    <?php
                        

                    }
                    else{
                        ?>
                            <a id="home" href="index.php">Home</a>
                            <br><br>
                            <a id="conn" href="connexion.php">Connexion</a>
                        <?php
                    }
                ?>
        </body>
    </html>
<?php    
}

function table_demande($item_sql){
    global $connexion_db;
    ?>
    <!DOCTYPE html>
        <html>
        <body>
        <style>
            table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            }
            th, td {
            padding: 15px;
            }
        </style>
       

        <table style="width:100%">
        <tr>
            <th colspan="2">Matériel</th>
            <th>Quantité</th> 
            <th>Ville</th>
        </tr>
        <?php
            $item_reponse = mysqli_query($connexion_db,$item_sql);
                while ($array_item = mysqli_fetch_array($item_reponse)){
                ?>
                <tr>
                    <td style="text-align:center"><?php echo $array_item['type_fil'];?></td>
                    <td style="text-align:center"><?php echo $array_item['fichier'];?></td>
                    <td style="text-align:center"><?php echo $array_item['quantite'];?></td>
                    <td style="text-align:center"><?php echo $array_item['ville'];?></td>
                </tr>
                <?php
                }
                ?>
        </table>

        </body>
        </html>
    <?php
}

?>