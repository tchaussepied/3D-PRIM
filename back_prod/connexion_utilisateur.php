<?php
    ini_set('display_errors', 1);
    require("fonctions.php");
    // session_destroy();
    
    
    require('connexionDB.php'); // Fichier PHP contenant la connexion à votre BDD
    global $connexion_db;

    $pseudo = $_POST['pseudo'];
    $mdp = $_POST['mdp'];

    $requete_sql = 'SELECT pseudo, mdp, idUtilisateur, acteur  FROM Utilisateur WHERE pseudo = "'.$pseudo.'";';
    $req_identification = mysqli_query($connexion_db,$requete_sql);
    $resultat = mysqli_fetch_row($req_identification);
    $mdpCorrect = password_verify($mdp, $resultat[1]);

    if (!$resultat){
        echo "Mauvais identifiant !";
        header('Location: index.php');        

    }
    else{
        if ($mdpCorrect){
            session_start();

            $_SESSION['id'] = $resultat[2];
            $_SESSION['pseudo'] = $pseudo;
            $_SESSION['acteur'] = $resultat[3];
            if ($_SESSION['acteur'] == "admin"){
                header('Location: admin.php');
            }
        }
        else{
            echo "Mauvais mot de passe !";
            header('Location: index.php');        

        }
    }

?>
