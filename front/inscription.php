<?php
    ini_set('display_errors', 1);
    require("http://144.91.89.57:8081/fonctions.php");
    onglets_menu_html();
?>


<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Inscription</title>

    </head>
    <body>      
        <div class="inscription"> 
        Inscription
            <form method="post" action="http://144.91.89.57:8081/enregistrement_inscription.php" name="inscription">
                    
                <label for="acteur">Rôle : </label>
                    <select name="acteur" required>
                        <option value="" selected="" > Votre rôle dans notre combat! </option>
                        <option value="demandeur"> Demandeur de masques </option>
                        <option value="maker"> Fabriquant de masques (maker)</option>
                        <option value="fournisseur"> Fournisseur de bobines </option>
                    </select><br>
                <label for="pseudo">Pseudo : </label>
                <input type="text" placeholder="Votre pseudo" name="pseudo" value="" required><br>
                <label for="nom">Nom : </label>
                <input type="text" placeholder="Votre nom" name="nom" value="" required><br>
                <label for="prenom">Prénom : </label>
                <input type="text" placeholder="Votre prénom" name="prenom" value="" required><br>
                <label for="mail">Mail : </label>
                <input type="email" placeholder="Adresse mail" name="mail" value="" required><br>
                <label for="telephone">Téléphone : </label>
                <input type="text" placeholder="Numero de téléphone" name="telephone" value=""><br>
                <label for="mdp">Mot de passe : </label>
                <input type="password" placeholder="Mot de passe" name="mdp" value="" required><br>
                <label for="confmdp">Confirmation du mot de passe : </label>
                <input type="password" placeholder="Confirmer le mot de passe" name="confmdp" required><br>
                <button type="submit" name="inscription">Envoyer</button><br>
            </form>
        </div>
    </body>
</html>