<?php
    ini_set('display_errors', 1);
    require("fonctions.php");
    session_start();
    onglets_menu_html();
    require('connexionDB.php');
    global $connexion_db;
?>

<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Utilisateur</title>
    </head>
    <body>
        <fieldset>
            <legend>A l'aide</legend>
            <?php
                demande($_SESSION['acteur']);
            ?>
        </fieldset>
        <fieldset>
            <legend>J'arrive à la rescousse </legend>
            <?php
                resoudre($_SESSION['acteur']);
            ?>
        </fieldset>
        <fieldset>
            <legend>Je suis prêt à donner</legend>
            <?php
                effectuer($_SESSION['id']);
            ?>
        </fieldset>
        <fieldset>
        <legend>Ce n'était pas si important ...</legend>
            <?php
                supprimer($_SESSION['id']);
            ?>
        </fieldset>
    </body>
</html>