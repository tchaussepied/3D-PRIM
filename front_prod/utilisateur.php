<?php
    ini_set('display_errors', 1);
    require("fonctions.php");
    session_start();
    onglets_menu_html();
    require('connexionDB.php');
    global $connexion_db;
?>

<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Utilisateur</title>
    </head>
    <body>
       
            <?php
                demande($_SESSION['acteur']);
   
                resoudre($_SESSION['acteur']);
    
                effectuer($_SESSION['id']);

                supprimer($_SESSION['id']);
            ?>
        </fieldset>
    </body>
</html>