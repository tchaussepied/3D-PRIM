<?php
    ini_set('display_errors', 1);

    function supprimer($idUti){
        global $connexion_db;
        $item_sql='SELECT * FROM Demande,Materiel,effectue WHERE effectue.idDemande = Demande.idDemande AND effectue.idUtilisateur = "'.$idUti.'" AND Materiel.idItem = Demande.idItem AND Demande.attribue = \'0\' AND Demande.approuve = \'1\';';
        $item_reponse = mysqli_query($connexion_db,$item_sql);
        $resultat = mysqli_fetch_row($item_reponse);
        if (!$resultat){
            return;
        }?>         
        <fieldset>
            <legend>Ce n'était pas si important ...</legend>
                <form method="post" action="traitement_supprime.php" name="supprime">
                <label for="supprime">Je n'ai plus besoin de : <br> </label>
                <?php 
                                
                    $item_reponse = mysqli_query($connexion_db,$item_sql);
                    while ($array_item = mysqli_fetch_array($item_reponse)){
                    ?>
                        <input type=radio value="<?php echo $array_item['idDemande'];?>"name="idDemande" required >
                        <label for="idDemande"><?php 
                        if ($array_item['fichier']){
                            echo $array_item['quantite']." visières de type ". $array_item['fichier']." dans la ville de ". $array_item['ville'];
                        }
                        else{
                            echo $array_item['quantite']." bobines de type ". $array_item['type_fil']." dans la ville de ". $array_item['ville'];
                        };?><br></label>
                    <?php
                    }
                    ?><br><br>
                <button type="submit" name="supprime">Je suis sûr de vouloir supprimer ma demande.</button><br>
                </form>
        </fieldset>

    
    <?php
    }

function effectuer($idUti){
    global $connexion_db;
    $item_sql='SELECT * FROM Demande,Materiel,repond WHERE repond.idDemande = Demande.idDemande AND repond.idUtilisateur = "'.$idUti.'" AND Materiel.idItem = Demande.idItem AND Demande.traitee = \'0\';';
    $item_reponse = mysqli_query($connexion_db,$item_sql);
    $resultat = mysqli_fetch_row($item_reponse);
    if (!$resultat){
        return;
    }
    ?>
        <fieldset>
            <legend>Je suis prêt à donner</legend>
                <form method="post" action="traitement_effectue.php" name="effectue">
                <label for="effectue">J'ai produit : <br> </label>
                <?php 
                                
                    $item_reponse = mysqli_query($connexion_db,$item_sql);
                    while ($array_item = mysqli_fetch_array($item_reponse)){
                    ?>
                        <input type=radio value="<?php echo $array_item['idDemande'];?>"name="idDemande" required >
                        <label for="idDemande"><?php 
                        if ($array_item['fichier']){
                            echo $array_item['quantite']." visières de type ". $array_item['fichier']." dans la ville de ". $array_item['ville'];
                        }
                        else{
                            echo $array_item['quantite']." bobines de type ". $array_item['type_fil']." dans la ville de ". $array_item['ville'];
                        };?><br></label>
                    <?php
                    }
                    ?><br><br>
                <button type="submit" name="effectue">Je suis prêt à donner ce matériel.</button><br>
                </form>
       </fieldset>
    
    <?php
}

function demande($role){
    global $connexion_db;
    $item_sql='SELECT * FROM Materiel WHERE fichier IS ';
    if ($role == "maker"){
        $item_sql.=' NULL;'; 
        $nom_item='type_fil';
    }
    elseif($role == "demandeur"){
        $item_sql.=' NOT NULL;';  
        $nom_item='fichier'; 
    }
    else{
        return;  
    }
    ?>
        <fieldset>
            <legend>A l'aide</legend>
            <form method="post" action="action_demande.php" name="demande">
                <label for="item">J'ai besoin de : </label>
                    <select name="item" size="1" style="width:150px" required> 
                        <?php 
                            
                            $item_reponse = mysqli_query($connexion_db,$item_sql);
                            while ($array_item = mysqli_fetch_array($item_reponse)){
                                ?>
                                
                                <option value="<?php echo $array_item['idItem'];?>"><?php echo $array_item[$nom_item]?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    <label for="quantité"> Quantité : </label>
                    <input type="number" placeholder="Quantité" name="quantite" min="1" max="1000" required>
                <?php
                    menu_ville();
                ?>
                    <br><br>
                    <button type="submit" name="demande">Faire la demande</button><br>
            </form>
        </fieldset>
    <?php
}


function resoudre($role){
    global $connexion_db;
    $item_sql='SELECT * FROM Materiel WHERE fichier IS ';
    if ($role == "fournisseur"){
        $item_sql.=' NULL;'; 
        $nom_item='type_fil';
    }
    elseif($role == "maker"){
        $item_sql.=' NOT NULL;';  
        $nom_item='fichier'; 
    }
    else{
        return;  
        }
    ?>
        <fieldset>
            <legend>J'arrive à la rescousse </legend>
            <form method="post" action="action_resoudre.php" name="fournisseur">
                <label for="item"> Je veux donner des : </label>
                <select name="item" size="1" style="width:150px" required> 
                    <?php 

                    $item_reponse = mysqli_query($connexion_db,$item_sql);
                    while ($array_item = mysqli_fetch_array($item_reponse)){
                        ?>
                        
                            <option value="<?php echo $array_item['idItem'];?>"><?php echo $array_item[$nom_item]?>
                            </option>
                        <?php
                    }
                    ?>
                </select>

                    
                <?php
                    menu_ville();
                ?>
                <br><br>
                <button type="submit" name="fournisseur">Faire la recherche</button><br>
            </form>
        </fieldset>

    <?php
}

function menu_ville(){
    ?>
        <label for="ville"> dans la ville de : </label>
        <select name="ville" placeholder="Ville" required>
            <option value="bourg">Bourg-en-Bresse (01)</option>
            <option value="laon">Laon (02)</option>
            <option value="moulins">Moulins (03)</option>
            <option value="digne">Digne (04)</option>
            <option value="gap">Gap (05)</option>
            <option value="nice">Nice (06)</option>
            <option value="privas">Privas (07)</option>
            <option value="charleville">Charleville-Mézières (08)</option>
            <option value="foix">Foix (09)</option>
            <option value="troyes">Troyes (10)</option>
            <option value="carcassonne">Carcassonne (11)</option>
            <option value="rodez">Rodez (12)</option>
            <option value="marseille">Marseille (13)</option>
            <option value="caen">Caen (14)</option>
            <option value="aurillac">Aurilac (15)</option>
            <option value="angouleme">Angoulême (16)</option>
            <option value="larochelle">La Rochelle (17)</option>
            <option value="bourges">Bourges (18)</option>
            <option value="tulle">Tulle (19)</option>
            <option value="ajaccio">Ajaccio (2A)</option>
            <option value="bastia">Bastia (2B)</option>
            <option value="dijon">Dijon (21)</option>
            <option value="saintbrieuc">Saint-Brieuc (22)</option>
            <option value="gueret">Guéret (23)</option>
            <option value="perigueux">Périgueux (24)</option>
            <option value="besancon">Besançon (25)</option>
            <option value="lille">Valence (26)</option>
            <option value="evreux">Evreux (27)</option>
            <option value="chartres">Chartres (28)</option>
            <option value="quimper">Quimper (29)</option>
            <option value="nimes">Nîmes (30)</option>
            <option value="toulouse">Toulouse (31)</option>
            <option value="auch">Auch (32)</option>
            <option value="bordeaux">Bordeaux (33)</option>
            <option value="montpellier">Montpellier (34)</option>
            <option value="rennes">Rennes (35)</option>
            <option value="chateauroux">chateauroux (36)</option>
            <option value="tours">Tours (37)</option>
            <option value="grenoble">Grenoble (38)</option>
            <option value="lons">Lons-le-Saunier (39)</option>
            <option value="montdemarsan">Mont-de-Marsan (40)</option>
            <option value="blois">Blois (41)</option>
            <option value="saintetienne">Saint-Etienne (42)</option>
            <option value="lepuyenvelay">Le Puy-en-Velay (43)</option>
            <option value="nantes">Nantes (44)</option>
            <option value="orleans">Orléans (45)</option>
            <option value="cahors">Cahors (46)</option>
            <option value="agen">Agen (47)</option>
            <option value="mende">Mende (48)</option>
            <option value="angers">Angers (49)</option>
            <option value="saintlo">Saint-Lô (50)</option>
            <option value="chalons">Châlons-en-Champagne (51)</option>
            <option value="chaumont">Chaumont (52)</option>
            <option value="laval">Laval (53)</option>
            <option value="nancy">Nancy (54)</option>
            <option value="barleduc">Bar-le-Duc (55)</option>
            <option value="vannes">Vannes (56)</option>
            <option value="metz">Metz (57)</option>
            <option value="nevers">Nevers (58)</option>
            <option value="lille">Lille (59)</option>
            <option value="beauvais">Beauvais (60)</option>
            <option value="alencon">Alençon (61)</option>
            <option value="arras">Arras (62)</option>
            <option value="clermont">Clermont-Ferrand (63)</option>
            <option value="pau">Pau (64)</option>
            <option value="tarbes">Tarbes (65)</option>
            <option value="perpignan">Perpignan (66)</option>
            <option value="strasbourg">Strasbourg (67)</option>
            <option value="colmar">Colmar (68)</option>
            <option value="lyon">Lyon (69)</option>
            <option value="vesoul">Vesoul (70)</option>
            <option value="macon">Mâcon (71)</option>
            <option value="lemans">Le Mans (72)</option>
            <option value="chambery">Chambéry (73)</option>
            <option value="annecy">Annecy (74)</option>
            <option value="paris">Paris (75)</option>
            <option value="rouen">Rouen (76)</option>
            <option value="melun">Melun (77)</option>
            <option value="versailles">Versailles (78)</option>
            <option value="niort">Niort (79)</option>
            <option value="amiens">Amiens (80)</option>
            <option value="albi">Albi (81)</option>
            <option value="montauban">Montauban (82)</option>
            <option value="toulon">Toulon (83)</option>
            <option value="avignon">Avignon (84)</option>
            <option value="larochesuryon">La-Roche-sur-Yon (85)</option>
            <option value="poitiers">Poitiers (86)</option>
            <option value="limoges">Limoges (87)</option>
            <option value="epinal">Epinal (88)</option>
            <option value="auxerre">Auxerre (89)</option>
            <option value="belfort">Belfort (90)</option>
            <option value="evry">Evry (91)</option>
            <option value="nanterre">Nanterre (92)</option>
            <option value="bobigny">Bobigny (93)</option>
            <option value="creteil">Créteil (94)</option>
            <option value="pontoise">Pontoise (95)</option>
        </select>
    <?php
}

function onglets_menu_html(){
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>3D-PRIM</title>
            <link rel="stylesheet" href="style.css">

        </head>
        <body>
            <header>
                <h1>3D-PRIM, le site</h1>
            </header>
           
            <div id="mySidenav" class="sidenav">
                <?php
                    if (isset($_SESSION['id']) AND isset($_SESSION['pseudo'])){
                        echo 'Bonjour ' . $_SESSION['pseudo'];
                        echo '<br>Vous êtes un '.$_SESSION['acteur'];
                        ?><br><br>
                        <a id="home" href="index.php">Home</a>
                        <br><br>
                        <a id="user" href="utilisateur.php">Votre page perso</a>
                        <a id="deco" href="deconnexion.php">Déconnexion</a>
                    <?php
                        

                    }
                    else{
                        ?>
                            <a id="home" href="index.php">Home</a>
                            <br><br>
                            <a id="conn" href="connexion.php">Connexion</a>
                            <a id="insc" href="inscription.php">Inscription</a>
                        <?php
                    }
                ?>
                <br><br>
                <a id="d_co" href="demande_cours.php">Demandes en cours</a>
                <a id="d_re" href="demande_resolue.php">Demandes resolues</a>
                <a id="about" href="about.php">Contactez-nous</a>
            </div>         
        </body>
    </html>
<?php    
}

function table_demande($item_sql){
    global $connexion_db;
    ?>
    <!DOCTYPE html>
        <html>
        <body>
        <style>
            table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            }
            th, td {
            padding: 15px;
            }
        </style>
       

        <table style="width:100%">
        <tr>
            <th colspan="2">Matériel</th>
            <th>Quantité</th> 
            <th>Ville</th>
        </tr>
        <?php
            $item_reponse = mysqli_query($connexion_db,$item_sql);
                while ($array_item = mysqli_fetch_array($item_reponse)){
                ?>
                <tr>
                    <td style="text-align:center"><?php echo $array_item['type_fil'];?></td>
                    <td style="text-align:center"><?php echo $array_item['fichier'];?></td>
                    <td style="text-align:center"><?php echo $array_item['quantite'];?></td>
                    <td style="text-align:center"><?php echo $array_item['ville'];?></td>
                </tr>
                <?php
                }
                ?>
        </table>

        </body>
        </html>
    <?php
}

?>