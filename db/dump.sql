-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Lun 11 Mai 2020 à 22:00
-- Version du serveur :  5.7.30-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mydb`
--
CREATE DATABASE IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mydb`;

-- --------------------------------------------------------

--
-- Structure de la table `Demande`
--

CREATE TABLE `Demande` (
  `idDemande` int(11) NOT NULL,
  `idItem` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL,
  `ville` varchar(45) DEFAULT NULL,
  `attribue` tinyint(1) DEFAULT NULL,
  `traitee` tinyint(1) DEFAULT NULL,
  `approuve` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Demande`
--

INSERT INTO `Demande` (`idDemande`, `idItem`, `quantite`, `ville`, `attribue`, `traitee`, `approuve`) VALUES
(7, 1, 10, 'rennes', 0, 0, 1),
(8, 1, 5, 'nantes', 0, 0, 1),
(18, 5, 10, 'rennes', 0, 0, 0),
(19, 5, 2, 'larochelle', 1, 1, 0),
(20, 5, 50, 'larochelle', 1, 0, 0),
(21, 5, 209, 'larochelle', 0, 0, 0),
(22, 5, 38, 'larochelle', 0, 0, 0),
(23, 5, 20, 'larochelle', 0, 0, 0),
(24, 5, 10, 'larochelle', 1, 1, 0),
(25, 5, 20, 'larochelle', 0, 0, 0),
(26, 5, 102, 'auxerre', 0, 0, 0),
(27, 5, 102, 'auxerre', 0, 0, 0),
(28, 5, 102, 'auxerre', 0, 0, 0),
(29, 1, 563, 'privas', 0, 0, 0),
(30, 5, 9000, 'macon', 0, 0, 0),
(31, 6, 1, 'bourg', 1, 0, 0),
(32, 7, 20, 'bourg', 1, 1, 0),
(33, 6, 10, 'troyes', 1, 0, 0),
(34, 7, 50, 'nice', 1, 1, 0),
(35, 5, 50, 'bourg', 1, 1, 0),
(36, 6, 99, 'bourg', 1, 1, 0),
(37, 6, 100, 'bourg', 0, 0, 0),
(38, 1, 50, 'bourg', 1, 0, 0),
(39, 1, 10, 'bourg', 0, 0, 0),
(40, 1, 10, 'bourg', 0, 0, 0),
(41, 6, 10, 'bourg', 0, 0, 0),
(42, 7, 10, 'bourg', 0, 0, 0),
(43, 7, 10, 'bourg', 0, 0, 0),
(46, 6, 10, 'bourg', 0, 0, 0),
(47, 1, 1000, 'bourg', 1, 0, 0),
(48, 1, 50, 'bourg', 0, 0, 0),
(49, 6, 10, 'bourg', 0, 0, 0),
(50, 6, 0, 'bourg', 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `effectue`
--

CREATE TABLE `effectue` (
  `idUtilisateur` int(11) NOT NULL,
  `idDemande` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `effectue`
--

INSERT INTO `effectue` (`idUtilisateur`, `idDemande`) VALUES
(6, 24),
(6, 25),
(6, 26),
(6, 27),
(6, 28),
(6, 29),
(8, 30),
(7, 31),
(7, 32),
(7, 33),
(7, 34),
(6, 35),
(7, 36),
(7, 37),
(6, 38),
(6, 39),
(6, 40),
(7, 41),
(7, 43),
(7, 46),
(10, 47),
(10, 48),
(11, 49),
(7, 50);

-- --------------------------------------------------------

--
-- Structure de la table `Materiel`
--

CREATE TABLE `Materiel` (
  `idItem` int(11) NOT NULL,
  `chemin` varchar(45) DEFAULT NULL,
  `fichier` varchar(45) DEFAULT NULL,
  `type_fil` varchar(20) NOT NULL,
  `stock_fil` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Materiel`
--

INSERT INTO `Materiel` (`idItem`, `chemin`, `fichier`, `type_fil`, `stock_fil`) VALUES
(1, 'modele_3D', 'masque_x1.stl', '', 0),
(5, 'modele_3D', 'masque_x5.stl', '', 0),
(6, NULL, NULL, 'PLA', 999),
(7, NULL, NULL, 'BET', 500);

-- --------------------------------------------------------

--
-- Structure de la table `repond`
--

CREATE TABLE `repond` (
  `idUtilisateur` int(11) NOT NULL,
  `idDemande` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `repond`
--

INSERT INTO `repond` (`idUtilisateur`, `idDemande`) VALUES
(7, 24),
(5, 32),
(5, 33),
(5, 34),
(7, 35),
(5, 36),
(5, 31),
(7, 38),
(11, 47);

-- --------------------------------------------------------

--
-- Structure de la table `Utilisateur`
--

CREATE TABLE `Utilisateur` (
  `idUtilisateur` int(11) NOT NULL,
  `pseudo` varchar(20) DEFAULT NULL,
  `mdp` varchar(200) DEFAULT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  `acteur` varchar(45) DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL,
  `telehone` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Utilisateur`
--

INSERT INTO `Utilisateur` (`idUtilisateur`, `pseudo`, `mdp`, `nom`, `prenom`, `acteur`, `mail`, `telehone`) VALUES
(1, 'pseudo', 'mdp', 'nom', 'prenom', 'maker', 'toto@toto', '012345678'),
(2, 'toto', 'yosso', 'chaussepied', 'thomas', 'maker', 'toto@gmail.com', NULL),
(4, 'Azerty', '', 'Polnareff', 'Jean Pierre', 'demandeur', 'jp.pol@orange.com', '0212365284'),
(5, 'aqw', '$2y$10$A1Jq.0ufoGAbp1v41LHmEOx3j17jEIsUxgD/x1Hw4tkUDh3cILizm', 'lelouche', 'maxime', 'fournisseur', 'ma@xime.com', ''),
(6, 'qsd', '$2y$10$.Y.LCi4NHNnd3GlQuQ8oEeHG3A4nCu/yDG32H4vGzj5Wej6ovsLjq', 'qsd', 'qsdfg', 'demandeur', 'qsd@qsd', 'qsd'),
(7, 'treza', '$2y$10$k1HLEpmkQ2oqJiJvE2d9hunAizgE4E/QmAHGB2xmE1yz0fAsdWT0i', 'lebuanec', 'tristan', 'maker', 't.lb@gmail.com', ''),
(8, 'liloudu35', '$2y$10$b808QBDBgNcteFWP0GaIp.V6hIIDYCmow0lvJG1WGtZ.xxJi1B9a6', 'du 35', 'lilou', 'demandeur', 'lilou@35.fr', ''),
(9, 'aaj', '$2y$10$m9ZjSJ5jrBEZBT9y.mAIe.KbtphD7GXYLRo1rgU3qVRrRU3PJbiCK', 'aaj', 'aaj', 'maker', 'aaj@aaj', ''),
(10, 'karim', '$2y$10$Oo1tdsnTFzo5/EYRAIw8l.fhiwVhVlvHj3bpBMChmE8RERo54.mNO', 'karim', 'karim', 'demandeur', 'karim@karim', ''),
(11, 'karim1', '$2y$10$1GumDjSbx4xGSW1A2JjVL.BRwV8QD2ckc9nNC8KP9D91tGg4Rhfhe', 'karim1', 'karim1', 'maker', 'karim1@karim1', ''),
(12, 'karim2', '$2y$10$R0Ifj/QhMcAvKwrfQdqc7OIzb4dFAbFZzmWPy7Bk/JYSIaQ5vX30y', 'karim2', 'karim2', 'fournisseur', 'karim2@karim2', ''),
(13, 'admin', '$2y$10$cu4OThgqI52awpJNHXRAKu.tulW0fn8o/7iQZe29R.zJ9dl26vije', NULL, NULL, 'admin', NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Demande`
--
ALTER TABLE `Demande`
  ADD PRIMARY KEY (`idDemande`),
  ADD KEY `fk_Demande_1_idx` (`idItem`),
  ADD KEY `fk_Demande_2_idx` (`idItem`) USING BTREE;

--
-- Index pour la table `effectue`
--
ALTER TABLE `effectue`
  ADD KEY `fk_effectue_1_idx` (`idUtilisateur`),
  ADD KEY `fk_effectue_2_idx` (`idDemande`);

--
-- Index pour la table `Materiel`
--
ALTER TABLE `Materiel`
  ADD PRIMARY KEY (`idItem`);

--
-- Index pour la table `repond`
--
ALTER TABLE `repond`
  ADD KEY `fk_resoud_1_idx` (`idUtilisateur`),
  ADD KEY `fk_resoud_2_idx` (`idDemande`);

--
-- Index pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD PRIMARY KEY (`idUtilisateur`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Demande`
--
ALTER TABLE `Demande`
  MODIFY `idDemande` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `Materiel`
--
ALTER TABLE `Materiel`
  MODIFY `idItem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  MODIFY `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Demande`
--
ALTER TABLE `Demande`
  ADD CONSTRAINT `fk_Demande_1` FOREIGN KEY (`idItem`) REFERENCES `Materiel` (`idItem`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `effectue`
--
ALTER TABLE `effectue`
  ADD CONSTRAINT `fk_effectue_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_effectue_2` FOREIGN KEY (`idDemande`) REFERENCES `Demande` (`idDemande`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `repond`
--
ALTER TABLE `repond`
  ADD CONSTRAINT `fk_resoud_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_resoud_2` FOREIGN KEY (`idDemande`) REFERENCES `Demande` (`idDemande`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
